
![Alt text](jpg/me.jpg "Picture of me")


## SUMMARY 

Andrzej has over 30 years of experience in IT customer support for small
business, corporations and banking industry. He has good practice in
creation of technical documentation, new functionalities design and
development. Last 11 years Andrzej has been working in ITIL oriented
teams, in projects concerning support for big corporate customers like:
Postnord, Fiat, Quad, Minolta. 
He has god practice writing commercial applications in Borland Delphi and
Lazarus Freepascal IDE.

  -----------------------------------------------------------------------
  **Sector's knowledge**              Banking, Manufacturing, Small
                                      Business
  ----------------------------------- -----------------------------------
  **Programming languages:**           PowerShell, Bash, TSQL, PLSQL, Delphi

  **Databases:**                       MSSQL, MYSQL, ORACLE, POSTGRES, ...

  **Operating systems:**               Windows, Linux

  **Version control systems:**         GitLab, GitHub

  **Project management                SCRUM, ITIL
  methodologies:**                     

  **Project management tools:**        Jira, ServiceNow, Cherwell, HPSM

  **Tools for software building and   Lazarus Free Pascal, Visual Studio,
  testing:**                           PowerShell, VirtualBox, Docker,
                                      Hyper-V, Jupyter, Jenkins, ...


  -----------------------------------------------------------------------


## PROFESSIONAL EXPERIENCE


### SII - PIŁA - 4 years
IT SUPPORT SPECIALIST at Roche Company
03.2020 - current

Project description & team size:

-   Third line BEE support for Roche Company.

Tasks:

-   Supporting more than 2000 Linux (RHEL) users and company
    applications.

-   Supporting Second Line Engineers.

-   Writing internal technical documentation.

-   Troubleshooting of Roche BEE environment.

-   Upgrades/decommissions/migrations/configurations.

-   New functionalities development.

-   Cooperation with QA team

-   Writing scripts in PowerShell or Bash for daily tasks.

-   Writing SQL applications in Delphi/Lazarus IDE.

-   Knowledge transfers and trainings

-   Migration of the users to dedicated environment in a cloud.

Technologies:

-   Bash, Python, R, PowerShell, SAS, JUPYTER, PLSQL, VMware, AWS, Delphi

### QUAD IT GLOBAL SOLUTIONS - 4 years
DATABASE ADMINISTRATOR
2016.07.01 - 2020.04.30

Project description & team size:

-   US team for SQL administration based tasks -- 20 people, 2 Scrum
    teams

Tasks:

-   Tasks regarding MSSQL servers and its computer operating systems
    (physical and virtual).

-   SQL Agent Jobs comparison and troubleshooting.

-   Database backups/restores (native, EMC Avamar, TSM).

-   Maintenance of High Compliance SQL Servers.

-   Linked Servers comparison and troubleshooting on primary and standby
    SQL servers.

-   Verification of long running transactions.

-   Database mirrors troubleshooting and maintenance.

-   High Availability Groups and Cluster Instances failover,
    maintenance, creation, troubleshooting.

-   Valid logins checking.

-   Creation of logins, resetting passwords for users.

-   Installations and upgrades of SQL Servers.

-   Writing PowerShell scripts for SQL.

-   Writing SQL queries for Automation Team.

-   Developing of small applications for SQL with clones of Delphi IDE.

-   Supporting Web Server Support Team ( IIS, Httpd, Tomcat, ...)

-   Maintenance and installation of MSSQL on Centos Linux.

-   Simple tasks for Oracle and MySQL servers.

-   Supporting other teams in SQL tasks.

> My previous position was: Second Line Support Engineer in EIO
> (Enterprise Infrastructure Operations) with scope inherited from other
> teams as follows:
>
> DIG (CC):

-   Proactive Sophos virus remediation

-   General workstation troubleshooting

-   Driver certification

-   NetMotion troubleshooting

-   Bitlocker troubleshooting

-   Bitlocker key recovery

-   SCCM reporting

-   SCCM collection management

-   SCCM device management

-   Remote laptop support

-   Re-Imaging troubleshooting

> WNS:

-   Create/move AD users

-   DHCP scope creation

-   DHCP reservations

-   PKI Certificates

-   Adding/removing targets in smartpass

-   Troubleshoot account lockouts

-   VOIP scope options for DHCP - (should be handled by WNS - it is now
    done by script)

-   NTP troubleshooting

-   ADFS Reliant Party Trusts

-   Modify/Add DNS records

-   Name monitoring in DNS

-   QG Guest account access

-   QG guest account extension

-   Approving Generic/Service accounts in Quoms

-   Approving AD group creation in Quoms

-   Modify AD group memberships in the Request system

-   NPS Radius Management

-   SmartPass troubleshooting

-   Handle Level 3's coming from WNS

-   Remove domains in EMT

-   Troubleshoot ADFS issues

> ECI:

-   Shares:

    -   Create New

    -   Modify Permissions

    -   Change Space Quotas

-   I drives:

    -   Change Space Quotas

    -   Move location

    -   Print Servers:

    -   Create New Queues

    -   Assist with Drivers

    -   Troubleshoot:

-   Ecopy:

    -   Add/Remove

    -   FM Audit/XDA Lite -- page counts

    -   Drive Clean up

-   VMware/Hyper-V:

    -   Reboot -- as requested

    -   Snapshot/Checkpoint

-   Gar:

    -   PreGAR Cleanup

    -   Schedule

    -   Monitor

    -   Host Patching

-   SCOM:

    -   View Performance History

    -   Edit Monitors

    -   Decommissions

-   ESReview/Account Access:

    -   Create New

    -   Grant/Modify access

-   Service Promotion:

    -   Alpha, Beta, Prod 3s &4s

-   MUW V2 Profile Deletion

-   Restore files from Symantec Backup Exec

-   Restore Files from Unitrends Virtual Appliance

-   Install and configure Vivotek IP cameras

-   Install and configure Geist Environmental Sensors

> Networks:

-   Switching:

    -   Vlan changes

    -   Trunking/Removing Vlans

    -   Port configuration

    -   IP/MAC tracing

    -   Replace closet switches that die (when we have time, otherwise
        send ticket to networks)

-   Wireless:

    -   AP configuration

    -   Wireless troubleshooting

-   Cisco Prime:

    -   Carrier Maintenance

    -   WAN outage tickets

-   NAC

-   Slowness troubleshooting

-   Solarwinds

> Compliance:

-   Audits for Deloitte

-   Internal Audits

> EUX:

-   Check File space on Linux File systems

-   Extend ext3/ext4 and xfs Linux filesystems

> WSS:

-   Monitoring:

    -   Uptime Robot (External)

    -   EMT (Internal)

    -   Servers (Application Pool Crash, CPU, Disk Space, ICMP, Memory,
        Trap Reboot)

-   Website Creation:

    -   WCF

    -   MVC

    -   Standalone Sites

    -   Test Websites prior to Production Deploy

    -   GIS APPS WSS

-   Load Configurator:

    -   Create Internal / External Sites on the F5 Load Balancers

-   Load Mover:

    -   Monitor Load on F5 Loadbalancers

    -   Pull Load on F5 Loadbalancers

-   Application Pools:

    -   Check Application Pool health status of Corporate WCF / MVC
        Farms

    -   Start / Stop Application Pools on Corporate WCF / MVC Farms

-   Healthchecks:

    -   Check health status of Corporate MVC / Websites

-   Certificates:

    -   Pull New Comodo Certificates

    -   Pull New PKI Certificates

    -   Attach Certificates to backend nodes

    -   Attach Certificates to F5 Load Balancer VIPs

-   AppDeploy:

    -   Create and Administer AppDeploy jobs

-   EMT:

    -   Create resources for MVC / WCF / Standalone Sites

    -   Pull IP addresses for Newly created servers and sites

    -   Creation and Removal of Domains

-   Domains:

    -   Purchase New Domains

    -   Maintain Domain Renewals

-   SSRS:

    -   Create New SSRS Folder

-   Add New Group to AD

-   AWSTATS:

    -   Create new Awstats resource

-   Sharepoint

-   Restores through Avamar

> MuW:

-   Create New Generic Accounts

-   Create New Thin Client Configurations

-   Delete/Recreate Roaming Profiles

-   Give Access to Published Applications

-   Respond to Low Disk Space Issues

-   Respond to Client Connection Issues

-   Restart Servers to Clear Issues

-   Respond to Printer Driver Issues

-   Respond to Citrix Profile Management issues

-   Respond to Level 3 Alerts

-   Respond to Application Issues

-   Work with users to reduce their profile size

Technologies:

-   As described above

### SWISSPOL PIŁA - 2 years
IT ADMINISTRATOR
2014.04 - 2016.07.01

Project description & team size:

-   One person IT support for mid-size international manufacturing
    company site in Piła.

Tasks:

-   Helpdesk tasks for users.

-   Maintenance, of physical and virtual windows servers (2008 server,
    HP ProLiant series), thinclients (HPT series), switches (mostly
    TPLINK), routers (Drytec), printers/photocopiers, computers (HP,
    IBM, DELL; Windows XP -- Windows 8.1) with: VPN, RDP, VNC, ILO,
    VirtualBox, ...

-   Administration of Exchange 2007 servers (mostly on SBS 2008).

-   Administration of other applications and devices in company
    according to ISO and ITIL standards.

-   Creation and maintenance of company website (swisspol.pila.pl).

-   Creation of corporate documents.

-   Organizing and taking part in meetings, trainings.

Technologies:

-   Bash, PowerShell, IIS, Apache, Sendmail, Samba, Joomla, PHP, MySQL,
    Exchange Server, MSSQL, Delphi

### ATOS BYDGOSZCZ - 1 year
SECOND LINE SUPPORT ENGINEER at Postnord and Atos Internal\
2013.04.15 - 2014.03.31

Project description & team size:

-   Two big international teams + 24h on call support

Tasks:

-   Resolving tickets related to windows servers NT4.0 -- 2008 (3
    shifts + on call 24/7).

-   Maintenance, troubleshooting of physical and virtual windows servers
    with: VPN, RDP, ILO, IDRAC, vSphere.

-   Virtualizations, decommissions, deployments, troubleshooting,
    estimations of servers.

-   Maintenance of: WSUS, WDS, IIS, DNS, DHCP, DFS, clusters, printers,
    \...

-   Dispatching and managing tickets from other engineers in short SLA
    international team.

-   Organizing and taking part in online meetings, trainings.

-   Creation of corporate documents.

Technologies:

-   PowerShell, VBS

### XERO-MAG - PIŁA
OWNER
1997.04.04 - current

-   Selling and installing computer networks, computers, servers,
    peripheral devices, computer components.

-   Administration of some NT4.0/2K/2003/2008; LINUX; NETWARE servers.

-   Configuring, administering AD, GPO, RDP, DNS, DHCP, WINS at w2000,
    w2003, w2008 mostly on HP ProLiant and Dell PowerEdge servers.

-   Implementing SharePoint, Outlook, OWA, SQL, IIS and Samba, Sendmail,
    Apache, MySQL, Firebird, ...

-   Outsourcing for IT companies in region.

-   Developing small apps (reports, conversions, printouts, editions,
    ...) with Lazarus Free Pascal.

-   Contracts with other companies for support.

-   Creation of webpages. (<http://jkmw-kotwica.pl>
    <http://swisspol.pila.pl>


### PILCAR FIAT ASO PIŁA - 7 years
NETWORK AND SYSTEM ADMINISTRATOR
2007.08.01 - 2013.04.15

Project description & team size:

-   One person IT support for automotive company

Tasks:

-   Administration of some old NetWare, Linux and samba servers,
    workstations, thin clients.

-   Administration of routers, printers, internet/intranet applications.

-   Software installations, actualizations, helpdesk for users

Technologies:

-   Novell Netware, Bash

### BANK PEKAO S.A. PIŁA - 4 years
SYSTEM ADMINISTRATOR
1998.09.14 - 2001.03.31

Project description & team size:

-   Three person team + oncall support for banking company

Tasks:

-   Administration of Novell Netware 4,11.

-   Administration of banking system Gryfbank.

-   Administration of teleinformatic system (ISDN, v34+modems, Cisco
    routers).

-   Administration of windows NT 4.0/2000 workstations and servers.

-   Administration of backup solutions (DLT, Maxoptics, Iomega).

-   Maintaining and installing computers and operating systems (nt 4.0,
    2000; 9\*; me).

-   Administration of banking gateway system, working with databases of
    Btrieve, Dbase.

-   Installing software, hardware, helpdesk for users.

-   Implementing security solutions (ISS, Webtrends, patches)

-   Closing the periods in a Bank Accounting Systems

Technologies:

-   Novell Netware, Accounting Systems, NT 4.0 server, heavy duty
    printers

### KSERO-SERWIS KRAKÓW - 5 years
Service technician 1993-1997

Project description & team size:

-   Service Technician in 20 people team for Minolta authorized dealer.

Tasks:

-   Maintaining/repairing analog and digital copying/printing devices
    (Minolta, Canon, Xerox, Ricoh, Nashua, Gestetner, ....

-   Installing: copiers, computers, software, extensions.

### EDUCATION

AGH University of Science and Technology Kraków

Field of study: Faculty of Electrical Engineering, Automatics, Computer
Science and Electronics

Engineer

10.03.2000

### CERTIFICATES & COURSES

VMware Certified Professional 5 Data center Virtualization -
Verification code: 12676720-9A84-5AACF032C55D,

Microsoft Certified Professional - Certification number: F311-8626,

Microsoft Certified Technology Specialist - Certification number:
F311-8628,

MS20764C: Administering a SQL Database Infrastructure,

Managing a Microsoft Windows Server 2003 Environment (MS 2274),

Implementing and Supporting Microsoft Windows XP Professional (MS 2272),

Administering a Microsoft SQL Server 2000 Database (MS 2072),

Configuring Managing and Maintaining Windows Server 2008 based Servers
(MS6419B),

Configuring and Troubleshooting Windows Server 2008 Active Directory
Domain Services (MS6425C),

Writing Queries Using Microsoft SQL Server 2008 Transact-SQL (MS2778),

Deploying Windows Server 2008 (MS6418),

Configuring and Managing Windows Media Services for Windows Server 2008
(MS6429),

Overview of Active Directory Rights Management Services with Windows
Server 2008 R2 (MS50404BC),

Red Hat RH300 RHCE Rapid Track Course,

VMware vSphere Fast Track 5.1,

CISCO CCNA 1: Networking Basics,

CISCO CCNA 2: Routers and Routing Basics,

CISCO CCNA 3: Switching Basics and Intermediate Routing,

CISCO CCNA 4: Wan technologies,

Internal trainings: ITIL, Lean, many technical trainings,

Driving License and others.

### FOREIGN LANGUAGES

-   Fluent English

-   Working knowledge of Russian

-   Basic knowledge of German

### PASSIONS & HOBBIES

-   Sailing
    > <https://globtourist.com/nasi-skipperzy/andrzej-mesojed/>

-   Ski touring / Freeride
